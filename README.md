# surround.vim for Pearl

surround.vim: quoting/parenthesizing made simple

## Details

- Plugin: https://github.com/tpope/vim-surround
- Pearl: https://github.com/pearl-core/pearl
